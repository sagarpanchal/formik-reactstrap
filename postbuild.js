const fse = require('fs-extra')

void (async () => {
  try {
    await new Promise(r =>
      fse.unlink('types', () => fse.mkdir('types', () => r()))
    )
    const list = (await fse.readdir('src')) || []
    list
      .filter(name => RegExp(/^[A-Za-z]+\.d\.ts$/).test(name))
      .forEach(name => fse.copyFile(`src/${name}`, `types/${name}`))
  } catch (error) {
    console.log({ PostBuildError: error })
  }
})()
