import React from 'react'
import { Formik, Form } from 'formik'
import Button from 'reactstrap/lib/Button'
import RenderFields from '../../src/RenderFields'
import { validationSchema, initialValues, fields } from './dataset'

const FormikExample = () => (
  <div className="row">
    <div className="col mx-auto pt-4 pb-3" style={{ maxWidth: '576px' }}>
      <Formik
        {...{ initialValues, validationSchema }}
        onSubmit={v => alert(JSON.stringify(v, null, 2))}
      >
        {formikProps => (
          <Form>
            <RenderFields {...{ fields, formikProps }} />
            <Button color="primary" size="sm" type="submit" name="submit">
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  </div>
)

export default FormikExample
