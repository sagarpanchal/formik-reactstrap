import * as Yup from 'yup'

const req = ' is required'

const ValidationSchema = {
  address: (fieldName = 'Address') => {
    return Yup.string()
      .trim()
      .min(12, 'Add more details')
      .required(fieldName + req)
      .typeError(fieldName + ' must be a string')
  },
  bool: (fieldName = 'Flag') => {
    return Yup.boolean()
      .required(fieldName + req)
      .typeError(fieldName + ' must be a boolean [true/false]')
  },
  email: (fieldName = 'Email') => {
    return Yup.string()
      .required(fieldName + req)
      .email(fieldName + ' is invalid')
      .typeError(fieldName + ' must be a string')
  },
  name: (fieldName = 'Name') => {
    return Yup.string()
      .strict(false)
      .trim()
      .min(2, fieldName + ' must be longer than 2 characters')
      .required(fieldName + req)
      .typeError(fieldName + ' must be a string')
  },
  password: (simple = false, fieldName = 'Password') => {
    const prefix = fieldName + ' must contain at least one'
    return simple
      ? Yup.string()
          .required(fieldName + req)
          .min(8, fieldName + ' must be at least 8 characters long')
          .max(16, fieldName + " can't be longer than 16 characters")
          .typeError(fieldName + ' must be a string')
      : Yup.string()
          .required(fieldName + req)
          .min(8, fieldName + ' must be at least 8 characters long')
          .max(16, fieldName + " can't be longer than 16 characters")
          .matches(/[A-Z]/, prefix + ' uppercase character')
          .matches(/[a-z]/, prefix + ' lowercase character')
          .matches(/\d/, prefix + ' number')
          .matches(/\W/, prefix + ' special character')
          .typeError(fieldName + ' must be a string')
  },
  phone: (fieldName = 'Phone') => {
    const message = fieldName + ' must be 10 characters long'
    return Yup.number()
      .min(1000000000, message)
      .max(9999999999, message)
      .required(fieldName + req)
      .typeError(fieldName + ' must be a number')
  },
  positiveInt: (fieldName = 'Number') => {
    return Yup.number()
      .min(0, fieldName + " can't be negative")
      .max(Number.MAX_SAFE_INTEGER)
      .required(fieldName + req)
      .typeError(fieldName + ' must be a number')
  },
  requiredString: (fieldName = 'Field') => {
    return Yup.string()
      .strict(false)
      .trim()
      .required(fieldName + req)
      .typeError(fieldName + ' must be a string')
  },
  fieldArray: (
    fieldName = 'Field',
    arrayOf,
    min = 0,
    max = Number.MAX_SAFE_INTEGER
  ) => {
    return Yup.array()
      .of(arrayOf)
      .required(fieldName + req)
      .min(min, 'Must have at least ' + min + ' ' + fieldName)
      .max(max, 'Must not have more than ' + max + ' ' + fieldName)
  }
}

export default ValidationSchema
