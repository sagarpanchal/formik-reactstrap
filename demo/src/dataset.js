import * as Yup from 'yup'
import VS from './ValidationSchema'

export const validationSchema = Yup.object().shape({
  address: VS.address(),
  birthdate: Yup.date()
    .required('Birth date is required')
    .typeError('Date is invalid'),
  city: VS.requiredString('City'),
  email: VS.email(),
  firstName: VS.name('First name'),
  gender: VS.requiredString('Gender'),
  lastName: VS.name('Last name'),
  phone: VS.fieldArray('Phones', VS.phone('Phone'), 1, 3),
  skills: VS.requiredString('Skills')
})

export const initialValues = {
  ...{ address: '', birthdate: '', city: '', email: '', firstName: '' },
  ...{ gender: '', lastName: '', skills: '', employed: false, phone: [''] }
}

export const fields = [
  {
    type: 'group',
    field: [
      { name: 'firstName', title: 'First name' },
      { name: 'lastName', title: 'Last name' }
    ]
  },
  { name: 'email', title: 'Email' },
  {
    type: 'group',
    field: [
      { name: 'birthdate', title: 'Birthdate', type: 'date' },
      { type: 'array', field: { name: 'phone', title: 'Phone' } }
    ]
  },
  {
    type: 'group',
    field: [
      {
        ...{ name: 'gender', title: 'Gender', type: 'select' },
        options: [
          { text: 'Male', value: 'male' },
          { text: 'Female', value: 'female' }
        ]
      },
      { name: 'city', title: 'City' }
    ]
  },
  { name: 'address', type: 'textarea', title: 'Address' },
  { name: 'skills', type: 'textarea', title: 'Skills' },
  { name: 'employed', type: 'switch', title: 'Employed' }
]
