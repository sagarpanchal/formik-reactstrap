import React from 'react'
import ReactDOM from 'react-dom'
import FormikExample from './FormikExample'
import './scss/app.scss'

const App = () => (
  <div className="container-fluid">
    <FormikExample />
  </div>
)

const rootElement = document.getElementById('demo')
ReactDOM.render(<App />, rootElement)
