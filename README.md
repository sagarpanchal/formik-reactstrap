# Formik components made with reactstrap

[![npm package][npm-badge]][npm]

[![codesandbox][codesandbox-badge]][codesandbox]

![alt][image]

# Getting Started

## Installing

```zsh
npm i formik yup bootstrap reactstrap formik-reactstrap
```

## Components

## &lt;Input/&gt; and &lt;InputArray/&gt;

```jsx
<Input type="text" name="name" title="Name" formikProps={props} />
```

```jsx
<InputArray type="text" name="names" title="Names" formikProps={props} />
```

### Props:

* `name: string`
* `title?: string`
* `type?: string`
* `formikProps: FormikProps<Values>`
* `className?: string`

## &lt;Select/&gt;

```jsx
<Select
  name="color"
  title="Color"
  options={[
    { text: 'Red'; value: '#f00' },
    { text: 'Green'; value: '#0f0' },
    { text: 'Blue'; value: '#00f' },
  ]}
  formikProps={props}
/>
```

### Props:

* `name: string`
* `title?: string`
* `options: <{ text: string; value: string }>[]`
* `formikProps: FormikProps<Values>`
* `className?: string`

## &lt;Switch/&gt;

```jsx
<Switch name="employed" title="Employed" formikProps={props} />
```

### Props:

* `name: string`
* `title?: string`
* `formikProps: FormikProps<Values>`

## &lt;Textarea/&gt;

```jsx
<Textarea name="description" title="Description" formikProps={props} />
```

### Props:

* `name: string`
* `title?: string`
* `formikProps: FormikProps<Values>`
* `className?: string`

[npm-badge]: https://img.shields.io/npm/v/formik-reactstrap?style=flat-square
[npm]: https://www.npmjs.org/package/formik-reactstrap
[codesandbox-badge]: https://codesandbox.io/static/img/play-codesandbox.svg
[codesandbox]: https://codesandbox.io/s/formik-reactstrap-example-oryed?fontsize=14&hidenavigation=1&module=%2Fsrc%2Fcomponents%2FExample%2Findex.js&theme=dark
[image]: https://gitlab.com/sagarpanchal/formik-reactstrap/raw/master/demo.png
