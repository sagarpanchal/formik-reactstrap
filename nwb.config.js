module.exports = args => {
  return {
    type: 'react-component',
    npm: { esModules: true, umd: false }
  }
}
