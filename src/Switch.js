import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import Error from './Error'
import Label from './Label'
import FormGroup from 'reactstrap/lib/FormGroup'
import _startCase from 'lodash/startCase'

export default class Switch extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.string,
    formikProps: PropTypes.object.isRequired
  }

  render() {
    const { name, title: fieldTitle, formikProps, ...rest } = this.props
    const { handleBlur, handleChange, values, touched, errors } = formikProps
    const title = !fieldTitle ? _startCase(name) : fieldTitle
    const checked = values[name]

    return (
      <FormGroup className="custom-control custom-switch">
        <Field
          component="input"
          {...{ name, id: name, checked, ...rest }}
          type="checkbox"
          className="custom-control-input"
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Label
          htmlFor={name}
          text={title}
          className="custom-control-label"
          style={{ cursor: 'pointer' }}
        />
        <Error touched={touched[name]} error={errors[name]} />
      </FormGroup>
    )
  }
}
