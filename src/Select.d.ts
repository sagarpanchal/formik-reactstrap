import * as React from "react";

declare type option = { text: string; value: string };

export default class Select extends React.Component<{
  name: string;
  title?: string;
  options: option[];
  formikProps: unknown;
  className?: string;
}> {}
