import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import FormGroup from 'reactstrap/lib/FormGroup'
import _startCase from 'lodash/startCase'
import moment from 'moment'
import Error from './Error'
import Label from './Label'

export default class Input extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.any,
    type: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    className: PropTypes.string
  }

  render() {
    const { name, title: fieldTitle, formikProps, ...rest } = this.props
    const { handleBlur, handleChange, values, touched, errors } = formikProps
    const value =
      rest.type === 'date' && values[name]
        ? moment(values[name]).format(moment.HTML5_FMT.DATE)
        : values[name]
    const title = !fieldTitle ? _startCase(name) : fieldTitle
    const errClass =
      (touched[name] && ((errors[name] && 'is-invalid') || 'is-valid')) || ''
    rest.className = `${(!rest.className && 'form-control') ||
      rest.className} ${errClass}`
    rest.type =
      rest.type ||
      (['email', 'password', 'date'].includes(name) ? name : 'text')

    return (
      <FormGroup>
        <Label htmlFor={name} text={title} />
        <Field
          {...{ name, value, ...rest }}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Error touched={touched[name]} error={errors[name]} />
      </FormGroup>
    )
  }
}
