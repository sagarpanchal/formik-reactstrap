import RenderFields from "./RenderFields";
import Input from "./Input";
import InputArray from "./InputArray";
import Select from "./Select";
import Switch from "./Switch";
import Textarea from "./Textarea";

export interface FormikReactstrap {
  RenderFields: RenderFields;
  Input: Input;
  InputArray: InputArray;
  Select: Select;
  Switch: Switch;
  Textarea: Textarea;
}
