import React from 'react'
import PropTypes from 'prop-types'
import Input from './Input'
import InputArray from './InputArray'
import Select from './Select'
import Switch from './Switch'
import Textarea from './Textarea'

export default class RenderFields extends React.Component {
  static propTypes = {
    fields: PropTypes.array,
    formikProps: PropTypes.object
  }

  render() {
    const { fields, formikProps } = this.props
    return fields.map((o, key) => {
      const { name, title, type, className, field: f, options } = o
      const p = { key, name, title, className, formikProps }

      switch (type) {
        case 'array':
          return <InputArray {...{ ...f, key, formikProps }} />

        case 'group':
          return (
            <div className="row" {...{ key }}>
              <div className="col-12 col-sm-6 field-sm-right">
                <RenderFields {...{ fields: [f[0]], formikProps }} />
              </div>
              <div className="col-12 col-sm-6 field-sm-left">
                <RenderFields {...{ fields: [f[1]], formikProps }} />
              </div>
            </div>
          )

        case 'select':
          return <Select {...{ ...p, options }} />

        case 'switch':
          return <Switch {...{ ...p }} />

        case 'textarea':
          return <Textarea {...p} />

        default:
          return <Input {...{ ...p, type }} />
      }
    })
  }
}
