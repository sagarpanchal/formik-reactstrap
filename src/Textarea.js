import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import Error from './Error'
import Label from './Label'
import FormGroup from 'reactstrap/lib/FormGroup'
import _startCase from 'lodash/startCase'

export default class Textarea extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.any,
    formikProps: PropTypes.object.isRequired,
    className: PropTypes.string
  }

  render() {
    const { name, title: fieldTitle, formikProps, ...rest } = this.props
    const { handleBlur, handleChange, values, touched, errors } = formikProps
    const title = !fieldTitle ? _startCase(name) : fieldTitle
    const value = values[name]
    const errClass =
      (touched[name] && ((errors[name] && 'is-invalid') || 'is-valid')) || ''
    rest.className = `${(!rest.className && 'form-control') ||
      rest.className} ${errClass}`

    return (
      <FormGroup>
        <Label htmlFor={name} text={title} />
        <Field
          component="textarea"
          {...{ name, value, ...rest }}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Error touched={touched[name]} error={errors[name]} />
      </FormGroup>
    )
  }
}
