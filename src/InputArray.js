import React from 'react'
import PropTypes from 'prop-types'
import { Field, FieldArray } from 'formik'
import FormGroup from 'reactstrap/lib/FormGroup'
import InputGroup from 'reactstrap/lib/InputGroup'
import InputGroupAddon from 'reactstrap/lib/InputGroupAddon'
import InputGroupText from 'reactstrap/lib/InputGroupText'
import _startCase from 'lodash/startCase'
import Error from './Error'
import Label from './Label'

export default class InputArray extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.any,
    type: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    className: PropTypes.string
  }

  render() {
    const { name, title: fieldTitle, formikProps, ...rest } = this.props
    const { handleBlur, handleChange, values, touched, errors } = formikProps
    const title = !fieldTitle ? _startCase(name) : fieldTitle
    const className = rest.className || 'form-control'
    rest.type =
      rest.type ||
      (['email', 'password', 'date'].includes(name) ? name : 'text')

    return (
      <FieldArray
        name={name}
        render={func => {
          const errClass =
            (touched[name] !== undefined &&
              errors[name] &&
              (typeof errors[name] === 'string' && 'is-invalid')) ||
            ''

          return (
            <React.Fragment>
              {values[name] && values[name].length > 0
                ? values[name].map((value, i) => {
                    const errClass =
                      (touched[name] !== undefined &&
                        (touched[name][i] &&
                          ((errors[name] !== undefined &&
                            (typeof errors[name] !== 'string' &&
                              (errors[name][i] &&
                                errors[name][i]['length'] > 1 &&
                                'is-invalid'))) ||
                            'is-valid'))) ||
                      ''

                    return (
                      <FormGroup key={i}>
                        <Label htmlFor={name} text={`${title} - ${i + 1}`} />
                        <InputGroup className={errClass}>
                          <Field
                            className={`${className} ${errClass}`}
                            name={`${name}.${i}`}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            {...{ value, ...rest }}
                          />
                          {errClass === 'is-invalid' && (
                            <Error touched={true} error={errors[name][i]} />
                          )}
                          <InputGroupAddon
                            addonType="append"
                            title="Remove"
                            onClick={() => func.remove(i)}
                          >
                            <InputGroupText
                              tag="button"
                              type="button"
                              className="form-control"
                            >
                              -
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                    )
                  })
                : null}
              <FormGroup className="cursor-pointer">
                <Label htmlFor={name} text={title} />
                <button
                  {...{ type: 'button', title: 'Add' }}
                  className={`cursor-pointer text-left ${className} ${errClass}`}
                  onClick={() => func.push('')}
                >
                  Add
                </button>
                {errClass === 'is-invalid' && (
                  <Error touched={true} error={errors[name]} />
                )}
              </FormGroup>
            </React.Fragment>
          )
        }}
      />
    )
  }
}
