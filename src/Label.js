import React from 'react'
import PropTypes from 'prop-types'
import RSLabel from 'reactstrap/lib/Label'

export default class Label extends React.Component {
  static propTypes = {
    htmlFor: PropTypes.string,
    text: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node)
    ])
  }

  focusInput = ({ target: { htmlFor } }) => {
    const field = document.querySelector(`[name='${htmlFor}']`)
    field !== null && field.focus()
  }

  render() {
    const { htmlFor, text: children, ...rest } = this.props
    return (
      <RSLabel
        {...{ htmlFor, onClick: this.focusInput, children, ...rest }}
        style={{ cursor: 'pointer' }}
      />
    )
  }
}
