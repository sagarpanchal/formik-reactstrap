import React from 'react'
import PropTypes from 'prop-types'
import FormFeedback from 'reactstrap/lib/FormFeedback'

export default class Error extends React.Component {
  static propTypes = {
    touched: PropTypes.bool,
    error: PropTypes.string
  }

  render() {
    const { touched, error } = this.props
    return touched && error ? (
      <FormFeedback
        valid={false}
        tag="span"
        tooltip
        aria-label="form-error"
        title={error}
      >
        {error}
      </FormFeedback>
    ) : null
  }
}
