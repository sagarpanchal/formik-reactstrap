import * as React from "react";

export default class Input extends React.Component<{
  name: string;
  title?: string;
  type?: string;
  formikProps: unknown;
  className?: string;
}> {}
